

//load data
var data = d3.csv("data/massmurders.csv", function (error, data) {
    if (error) throw error ;

var map, myLayer;

//map stuff

var pageW = $(document).width(),
  zoomLevel, rangeMax;

if (pageW<500){
  zoomLevel = 3;
  rangeMax = 1;
} else {
  zoomLevel = 4;
  rangeMax = 2;
}

L.mapbox.accessToken = 'pk.eyJ1IjoiZ3JhY2Vkb25uZWxseSIsImEiOiIwZzc2a01FIn0.9gJRwPR6RpFXn4-3CymaNw';
map = L.map('map', {center: [38.7466354,-99.3258327], zoom: zoomLevel});

L.mapbox.tileLayer('mapbox.light').addTo(map);
myLayer = L.mapbox.featureLayer().addTo(map);

// dataTables
  var table = $('#myTable'),
    tbody = $('<tbody></tbody>');

    
var temp = _.template('<tr><td><%= Title %></td><td><%= Location %></td><td><%= Description %></td><td><%= Date_Detailed %></td><td><%= Shooter_Name %></td><td><%= Shooter_Age %></td><td><%= Shooter_Sex %></td><td><%= Shooter_Race %></td><td><%= Number_of_Victim_Fatalities %></td><td><%= Number_of_Victims_Injured %></td><td><%= Total_Number_of_Victims %></td><td><%= Total_Number_of_Guns %></td><td><%= Type_of_Gun_Detailed%></td><td><%= Fate_of_Shooter%></td><td><%= Place_Type%></td><td><%= Relationship_to_Incident_Location%></td><td><%= Targeted_Victim_Detailed%></td><td><%= Possible_Motive_Detailed%></td><td><%= History_of_Mental_Illness_General%></td></tr>');

//larger circles need to be on the bottom so you can click the smaller ones that would be covered up if they were on top. SVG doesn't do z-index
data.sort(function(a, b){
   return d3.descending(+a.Total_Number_of_Victims, +b.Total_Number_of_Victims);
});

var rad = d3.scale.sqrt().domain([0, 1]).range([0, rangeMax]);

_.each(data, function(d){
    var row = temp(d);
    tbody.append(row);
    var colors = { "School": '#d33141 ', "Government facility": '#f4dd5e ', "Place of worship": '#477fc1 ', "Street/Highway": '#279969 ', "Office/Place of business": '#ef9b20 ', "Residence": '#9924ed ', "Medical/Care facility": '#E673AC ', "Other": '#7a7a7a ' };
    var content = '<div class="title clearfix"><span class="bold left h2">' + d.Title +'</span><span class="date right">' + d.Shooting_Date + '</span></div><div class="clearfix"><p>Total Victims: ' + d.Total_Number_of_Victims + '</p> <p>Victim fatalities: ' + d.Total_Number_of_Fatalities +'</p><p>' + d.Description + '</p></div>' ;
    var lat_long = d.ajc_lat_long.split(",");

    L.circleMarker([+lat_long[0], +lat_long[1]]).addTo(map).setRadius(rad(d.Total_Number_of_Victims)).setStyle({color: colors[placeFix(d)] })
        .on('click', function(event) {
          event.target.openPopup();
        }).bindPopup(content);
});

table.append(tbody);

var datainit = false;

$(document).foundation({
    tab: {
      callback : function (tab) {
        if (tab[0].id==='datasetTab' && !datainit){
          table.DataTable({ responsive: true});
          datainit = true;

        }
      }
    }
  });

  function placeFix(d){
    var placeT = d.Place_Type,
    sch_dict = ["Primary school", "Secondary school", "College/University/Adult education"],
    biz_dict = ["Retail/Wholesale/Services facility", "Restaurant/Cafe?", "Company/Factory/Office"],
    rez_dict = ["Residential home/Neighborhood"],
    med_dict = ["Medical/Care", "Hospital"],
    other_idct = ["Public transportation", "Park/Wildness", "Park", "Airport", "Public library"];
    type_dict = [["School", sch_dict], ["Office/Place of business", biz_dict], ["Residence", rez_dict], ["Medical/Care facility", med_dict]];
    //todo add ["Entertainment venue", "Nightclub"] category to colors
    //todo should "Street/college" go under streets? It's the Dallas police shooting at a protest

    for (var i=0; i<type_dict.length; i++) {
      if(type_dict[i][1].indexOf(placeT) >= 0){
        return type_dict[i][0];
      }
    }
    return d.Place_Type;
  }

  var viz_data = data.map(function(d){
    return { //lets rewrite the viz_data and organize it
      details: {
        title: d.Title,
        location: d.Location,
        description: d.Description,
        date: d.Date,
        place: placeFix(d)
      },
      shooter: {
        name: d.Shooter_Name,
        age: +d.Average_Shooter_Age,
        gender: d.Shooter_Sex,
        race: d.Shooter_Race,
        shooter_fate: function(){
          if (d.Fate_of_Shooter==='Dead'){
            return d.Shooters_Cause_of_Death
          } else {
            return d.Fate_of_Shooter
          }
        },
        death_cause: d.Shooters_Cause_of_Death,
        motive: d.Possible_Motive_General,
        motive_detail: d.Possible_Motive_Detailed,
        mental_illness: d.History_of_Mental_Illness_General,
        mental_illness_detail: d.History_of_Mental_Illness_Detailed
      },
      gun: {
        detail: d.Type_of_Gun_Detailed,
        shotgun: +d.Number_of_Shotguns,
        rifle: +d.Number_of_Rifles,
        handgun: +d.Number_of_Handguns,
        auto: +d.Number_of_Automatic_Guns,
        semi: +d.Number_of_SemiAutomatic_Guns
      }
    };
  });
  

  var shooter_data = data.map(function(d){
    return {
        name: d.Shooter_Name,
        id: d.CaseID,
        title: d.Title,
        location: d.Location,
        description: d.Description,
        date: d.Date,
        place: placeFix(d),
        age: function(){
          if (+d.Average_Shooter_Age>0) {
            return +d.Average_Shooter_Age
          } else {
            return '100'
          }
        },
        age_list: d.Shooter_Age,
        gender: d.Shooter_Sex,
        race: d.Shooter_Race,
        shooter_fate: function(){
          if (d.Fate_of_Shooter==='Dead'){
            return d.Shooters_Cause_of_Death
          } else {
            return d.Fate_of_Shooter
          }
        },
        death_cause: d.Shooters_Cause_of_Death,
        motive: d.Possible_Motive_General,
        motive_detail: d.Possible_Motive_Detailed,
        mental_illness: d.History_of_Mental_Illness_General,
        mental_illness_detail: d.History_of_Mental_Illness_Detailed
      }
  });


  var person = '<i class="fa fa-male"></i>'; //lil person icon

  var active = $('.shooter-buttons a.active').attr('id'); //active shooter button

  fillIcons(active);


  $('.shooter-buttons a').click(function(){
    active = $(this).attr('id');
    $('.shooter-buttons a').not(this).removeClass('active');
    $(this).addClass('active');
    fillIcons(active);
  });


  var sorted;

  function fillIcons(active){
    $('#shooter-div').empty();
    if (active==='age'){
      // var filtered_ages = _.filter(shooter_data, function(d){ return d.age>0 });
      sorted = _.sortBy(shooter_data, function(d){ return d.age() });
    } else if (active==='shooter_fate') {
      sorted = _.sortBy(shooter_data, function(d){ return d.shooter_fate() });
    } else if (active!='shooter_fate')  {
      sorted = _.sortBy(shooter_data, function(d){ return d[active] });
    } 

  sorted.forEach(function(d,i){
    if (d[active]){
      $('#shooter-div').append('<div class="person" id="'+d.id+'">'+person+'</div>');
    }
    if (active==='age'){
      if (d.age) {
        $('#'+d.id+' i')
          .css({'color': ageRange(d.age())})
          .mousemove(function(){

            $('#tooltip').show();
            //Get this bar's x/y values, then augment for the tooltip
            var xPos = event.clientX + document.body.scrollLeft;     // Get the horizontal coordinate
            var yPos = event.clientY + document.body.scrollTop-60; 
            //Update Tooltip Position & value
            $("#tooltip")
              .css("left", xPos)
              .css("top", yPos)
              .html('<strong>'+d.title+'</strong><br/>'+d.name+'<br/>Age: '+d.age_list);
          })
          .mouseout(function(event) {
            $('#tooltip').hide();
          });
      } else {
        $('#'+d.id+' i')
        .css({'color': '#aaa'})
        .mousemove(function(){

          $('#tooltip').show();
          //Get this bar's x/y values, then augment for the tooltip
          var xPos = event.clientX + document.body.scrollLeft;     // Get the horizontal coordinate
          var yPos = event.clientY + document.body.scrollTop; 
          //Update Tooltip Position & value
          $("#tooltip")
            .css("left", xPos)
            .css("top", yPos)
            .html('<strong>'+d.title+'</strong><br/>'+d.name+'<br/>Age: unknown');
          })
          .mouseout(function(event) {
            //Remove the tooltip
            $('#tooltip').hide();
          });
        }
      }
      if (active==='shooter_fate'){
        $('.person').css({'color': '#fff'})
        var color = colorIcons(d.shooter_fate(),active,sorted);
        $('#'+d.id+' i')
        .css({'color':color})
        .mousemove(function(){

          $('#tooltip').show();
          //Get this bar's x/y values, then augment for the tooltip
          var xPos = event.clientX + document.body.scrollLeft;     // Get the horizontal coordinate
          var yPos = event.clientY + document.body.scrollTop; 
          //Update Tooltip Position & value
          $("#tooltip")
            .css("left", xPos)
            .css("top", yPos)
            .html('<strong>'+d.title+'</strong><br/>'+d.name+'<br/>Shooter fate: '+ d.shooter_fate());
        })
        .mouseout(function(event) {
          //Remove the tooltip
          $('#tooltip').hide();
        });
      }
      if (active!='shooter_fate'&&active!='age') {
        $('.person').css({'color': '#fff'})
        var color = colorIcons(d[active],active,sorted);
        var activeText = active.split('_').join(' ');
        var details = active+'_detail';
        $('#'+d.id+' i')
          .css({'color':color})
          .mousemove(function(){
            $('#tooltip').show();
            //Get this bar's x/y values, then augment for the tooltip
            var xPos = event.clientX + document.body.scrollLeft;     // Get the horizontal coordinate
            var yPos = event.clientY + document.body.scrollTop; 
            //Update Tooltip Position & value
            $("#tooltip")
              .css("left", xPos)
              .css("top", yPos);

            if (d[details]){
              $("#tooltip").html('<strong>'+d.title+'</strong><br/>'+d.name+'<br/>'+activeText.capitalizeFirstLetter()+': '+ d[active] + '<br/>' + d[details]);
            } else {
              $("#tooltip").html('<strong>'+d.title+'</strong><br/>'+d.name+'<br/>'+activeText.capitalizeFirstLetter()+': '+ d[active]);
            }
          })
          .mouseout(function(event) {
            //Remove the tooltip
            $('#tooltip').hide();
          });
      }

    });
    makeLegend(active,sorted);
  }




  var minAge = d3.min(viz_data.map(function(d){ if(d.shooter.age){ return d.shooter.age } } ));
  var maxAge = d3.max(viz_data.map(function(d){ if(d.shooter.age){ return d.shooter.age } } ));
  $('p#minAge').append(minAge);
  $('p#maxAge').append(maxAge);


// '#AA3939','#FFCDCD','#DE7E7E','#760C0C','#430000'


  function ageRange(d){

    var colors = d3.scale.quantize().range(['#FFCBCB','#E28A88','#B84D4D','#8D1E1E']);
    colors.domain([d3.min(shooter_data, function(d){ return d.age() }), 60]);
    if (d<100) {
      return colors(d);
    } else {
      return '#cccccc'
    }
  };


  var values;
  var colors = {
    'gender': ['#d33141','#477fc1','#279969','#a7a7a7'],
    'race': ['#477fc1','#d33141','#ef9b20','#279969','#b7b7b7','#9924ed'],
    'shooter_fate': ['#477fc1','#d33141','#ef9b20','#279969','#9924ed','#7a7a7a'],
    'mental_illness': ['#477fc1','#d33141','#279969']
  };

  function colorIcons(d,id,sorted){
    //get all unique values for attribute
    if (id!='shooter_fate'){
      values = _.unique(d3.map(sorted, function(_d){return _d[id] }).keys());
    } else {
      values = _.unique(d3.map(sorted, function(_d){return _d.shooter_fate() }).keys());
    }

    var index = _.indexOf(values, d);
    var color = chroma(colors[id][index]).brighten(5).hex();
    return color;
  };


  function makeLegend(active,sorted){
    if (active==='age'){
      $('.shooter-legend').hide();
      $('#age-scale').show();
    }
    if (active!='age') {
      $('#age-scale').hide();
      $('.shooter-legend').show().empty();
      if (active!='shooter_fate'){
        values = _.unique(d3.map(sorted, function(_d){return _d[active] }).keys());
      } else {
        values = _.unique(d3.map(sorted, function(_d){return _d.shooter_fate() }).keys());
      }
      // values = unique_sort(values);
      $('.shooter-legend').append('<ul id="legend-grid" class="medium-block-grid-'+values.length+'"></ul>');
      $colorArr = colors[active];

      // $legendItem = '<i class="fa fa-male legend" style="color:'+chroma(colors[active][i]).brighten(10)+'"></i> ';
      values.forEach(function(v,i){
        $('ul#legend-grid').append('<li/>');
        $('ul#legend-grid li:last-child').append('<i class="fa fa-male legend"></i> '+v);
        $('ul#legend-grid li:last-child i').css({'color':chroma($colorArr[i]).brighten(10)});
      });

    }
  }



  // guns to come later
  $('#gun-grid li').click(function(){
    $id = $(this).attr('id');
    $('#gun-grid li').animate({'opacity':'0.5'},'fast');
    $(this).animate({'opacity':'1'},'fast');

    $('.gun-detail-row').each(function(){
      if ($(this).hasClass($id)){
        $(this).show();
      } else{
        $(this).hide();
      }
    });
  });

  viz_data.forEach(function(d,i){
    $gun = d.gun;
    $('#gun-details').append('<div class="gun-detail-row" id="shooting'+i+'"></div>');
    $('#shooting'+i).append('<div class="row"><div class="medium-5 columns"><p>'+d.details.title+'</p><div class="gun-details">'+$gun.detail+'</div></div><div class="medium-7 columns"><div class="gun-icons"></div></div></div>');
    // $('#shooting'+i).append('<div class="row"><div class="medium-12 columns"><div class="gun-details">'+$gun.detail+'</div></div></div>')

    $guns = _.omit($gun,'detail');
    for (var prop in $guns) {
      drawGunRow(i,prop,$guns[prop]);
    }
  });

  function filterForGun(gun){
    return _.reject(viz_data, function(d){ return +d.gun[gun]===0; });
  }

  function drawGunRow(i,gun,total){
    if (total!=NaN){
      for (var j=0;j<total;j++){
        $('#shooting'+i+' .gun-icons').append('<img src="images/'+gun+'.png"/>');
        $('#shooting'+i).addClass(gun);
      }
    }
  }








  String.prototype.capitalizeFirstLetter = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
  }


   // tabs

});

