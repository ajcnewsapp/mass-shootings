# My Project

Description:

Published:

Lives here:

Hosted here:

We were using another source but switched to [Mother Jones](https://docs.google.com/spreadsheets/d/1b9o6uDO18sLxBqPwl_Gh9bnhW-ev_dABH83M5Vb5L8o/edit#gid=0) (Original sheet we forked is actually [here](https://docs.google.com/spreadsheets/d/1XV4mZi3gYDgwx5PrLwqqHTUlHkwkV-6uy_yeJh3X46o/edit#gid=0) but that other one is the currently maintained version) because the other went away. [Here](https://docs.google.com/spreadsheets/d/1hJxTK1V_IDXAe4Y-ezCEiDlkLCEkwQstMBv74Xt0Ahk/edit#gid=0) is our fork of their data, where we have updated the coordinates because they were missing since 2015.
NOTE: The actual spreadsheet to update is in the repo - I don't really know why we are referencing the mother jones fork?
NOTE: For some reason the Mother Jones coordinates seem to frequently be way off when they have them, like not even in the right city, so you should probably double-check the coords for each instance you use

## Installation
- `npm install`
- `bower install`
- `grunt bowercopy`

##Deploy
- `grunt server`, take a new screenshot of map, put it in `src/images/`  and swap the URL in `og:image` metadata
- `grunt build`
- `grunt deploy`

